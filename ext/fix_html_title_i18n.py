# Workaround for https://github.com/sphinx-doc/sphinx/issues/1260
from sphinx.locale import _


def translate_html_title(app, config):
    config.html_title = _(config.html_title)


def setup(app):
    app.connect("config-inited", translate_html_title)
